from dependency_injector.wiring import Provide, inject
from fastapi import APIRouter, Depends, status

from app.internal import services
from app.pkg import models

router = APIRouter(prefix="/transactions", tags=["Transactions"])


@router.post(
    "/",
    status_code=status.HTTP_201_CREATED,
    summary="Transaction was successful.",
)
@inject
async def transaction(
        cmd: models.Transaction,
        transaction_service: services.Transactions = Depends(Provide[services.Services.transactions])
):
    return await transaction_service.create_transaction(cmd=cmd)

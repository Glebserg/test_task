import json

import pika


async def publish_message(message):
    transaction_str = message
    transaction = json.loads(transaction_str)

    # credentials = pika.PlainCredentials('user', '1234')
    # parameters = pika.ConnectionParameters(host='rabbit', credentials=credentials)
    publisher_connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    publisher_channel = publisher_connection.channel()

    # stable queue (will work after reboot)
    publisher_channel.queue_declare(queue='transaction_queue', durable=True)

    message = str(transaction)
    publisher_channel.basic_publish(
        exchange='',
        routing_key='transaction_queue',
        body=message,
        properties=pika.BasicProperties(
                             delivery_mode=2,  # make message persistent
                          )
    )

    print(" [x] Transaction sent %r" % (message,))
    publisher_connection.close()

import asyncio
import json

import aio_pika

from app.internal.repository.postgresql.transactions import Transactions
from app.pkg import models


async def read_message():
    # AMQP URL to connect to
    AMQP_URL = "amqp://guest:guest@localhost/"

    # Creating connection object
    connection = await aio_pika.connect_robust(AMQP_URL)

    # Creating a channel
    channel = await connection.channel()

    # Declare a queue
    queue = await channel.declare_queue('transaction_queue', durable=True)

    async def callback(message: aio_pika.IncomingMessage):
        async with message.process():
            body = message.body.decode('utf-8').replace("'", '"')
            msg_dict = json.loads(body)
            try:
                transaction = Transactions()
                res = await transaction.update_balance(
                    cmd=models.Transaction(
                        name=msg_dict["name"],
                        transaction=msg_dict["transaction"]
                    )
                )
            except Exception as e:
                print(e)
            print(f" [X] Transaction done.\n [*] INFO: {res}")

    return await queue.consume(callback)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(read_message())

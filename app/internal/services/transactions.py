import json

from app.internal.repository.repository import BaseRepository
from app.internal.repository.postgresql import transactions
from app.internal.services.rabbitmq.publisher import publish_message
from app.internal.services.rabbitmq.worker import read_message
from app.pkg import models
from app.pkg.models.exceptions.repository import EmptyResult, DriverError
from app.pkg.models.exceptions import InsufficientFunds, UserNotFound


__all__ = [
    "Transactions",
]


class Transactions:

    repository: transactions.Transactions

    def __init__(self, repository: BaseRepository):
        self.repository = repository

    async def create_transaction(self, cmd: models.Transaction) -> str:
        try:
            balance = await self.repository.check_balance(query=cmd)
            if balance.balance + cmd.transaction < 0:
                raise InsufficientFunds
        except EmptyResult:
            raise UserNotFound

        try:
            transaction = {'name': cmd.name, 'transaction': cmd.transaction}
            transaction_str = json.dumps(transaction)
            await publish_message(transaction_str)
        except Exception as e:
            print(e)

        try:
            return await read_message()
        except Exception as e:
            print(e)

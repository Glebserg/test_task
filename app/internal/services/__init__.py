from dependency_injector import containers, providers

from app.internal.repository import Repositories, postgresql
from app.internal.services.transactions import Transactions
from app.pkg.settings import settings


class Services(containers.DeclarativeContainer):
    """Containers with services."""

    configuration = providers.Configuration(
        name="settings",
        pydantic_settings=[settings],
    )

    repositories: postgresql.Repositories = providers.Container(
        Repositories.postgres,
    )  # type: ignore

    transactions = providers.Factory(Transactions, repositories.transactions)

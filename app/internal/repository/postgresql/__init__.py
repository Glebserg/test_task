from dependency_injector import containers, providers

from .transactions import Transactions


class Repositories(containers.DeclarativeContainer):
    transactions = providers.Factory(Transactions)

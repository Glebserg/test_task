from app.internal.repository.postgresql.handlers.collect_response import collect_response
from app.internal.repository.postgresql.cursor import get_cursor
from app.internal.repository.repository import Repository
from app.pkg import models

__all__ = [
    "Transactions",
]


class Transactions(Repository):
    @collect_response
    async def check_balance(self, query: models.Transaction) -> models.Balance:
        q = """
            SELECT balance FROM public.users
            WHERE name = %(name)s;
            """
        async with get_cursor() as cur:
            await cur.execute(q, query.to_dict())
            return await cur.fetchone()

    @collect_response
    async def update_balance(self, cmd: models.Transaction) -> models.BalanceAfterTransaction:
        q = """
            UPDATE public.users 
            SET balance = balance + %(transaction)s
            WHERE name = %(name)s
            returning id, name, balance;
            """
        async with get_cursor() as cur:
            await cur.execute(q, cmd.to_dict())
            return await cur.fetchone()

from pydantic.fields import Field
from pydantic.types import PositiveInt

from app.pkg.models.base import BaseModel

__all__ = [
    "TransactionFields",
    "BaseTransaction",
    "Transaction",
    "BalanceAfterTransaction",
    "Balance",
]


class TransactionFields:
    id = Field(description="users id", example=42)
    name = Field(description="users login", example="NickName")
    balance = Field(description="users balance", example=10000)
    transaction = Field(description="transaction amount", example=-100)


class BaseTransaction(BaseModel):
    ...


class Transaction(BaseTransaction):
    name: str = TransactionFields.name
    transaction: int = TransactionFields.transaction


class BalanceAfterTransaction(BaseTransaction):
    id: PositiveInt = TransactionFields.id
    name: str = TransactionFields.name
    balance: PositiveInt = TransactionFields.balance


class Balance(BaseTransaction):
    balance: PositiveInt = TransactionFields.balance

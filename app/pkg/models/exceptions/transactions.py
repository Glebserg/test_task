from fastapi import status

from app.pkg.models.base import BaseAPIException


__all__ = [
    "InsufficientFunds"
]


class InsufficientFunds(BaseAPIException):
    message = "Insufficient funds in the account."
    status_code = status.HTTP_423_LOCKED

from fastapi import status

from app.pkg.models.base import BaseAPIException


__all__ = [
    "UserNotFound"
]


class UserNotFound(BaseAPIException):
    message = "User with the same name not found."
    status_code = status.HTTP_404_NOT_FOUND

# Тестовое задание
С использованием:
- fastAPI
- PostgresQL
- RabbitMQ
- Docker

## Для запуска проекта

- Активировать виртуальное окружение и установить зависимости
```
poetry shell
poetry install
```
- Скопировать содержимое .env.example в .env
- Создать БД postgres с данными из .env
- Использовать скрипт из файла init.sql для создания таблицы и данных
- Запустить проект коммандой 
```
uvicorn app:create_app --reload
```
- [ ] [Открыть swagger](http://127.0.0.1:8000/docs) 

## TODO
- поправить docker-compose для запуска одной командой всего проекта
- доработать очередь rabbitMQ
- реализовать регистрацию\аутентификацию
- добавить возможность перевода между пользователями

-- Deleting the table if it exists
CREATE DATABASE test_task if not exists;

-- Create table users
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) UNIQUE NOT NULL,
  balance int default 100 NOT NULL
);

-- Adding data to the table
INSERT INTO users (
    name
)
VALUES (
    'NickName'
);